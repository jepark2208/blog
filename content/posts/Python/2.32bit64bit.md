---
title: "1"
description: ""
date: "2021-08-06T08:51:55+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---

https://all-share-source-code.tistory.com/23 참고

1. 32bit 먼저 설치

2. 64bit 먼저 설치

### 1. 2. 공통 설정
  - Add Python 3.9 to PATH 체크 -> 커스텀 인스톨
  - pip, tcl/tk, for all users 3갬 만 체크 후 넥스트
  - 모두 체크 후 인스톨
  - 설치경로에 가서 python.exe -> python32.exe | python64.exe 로 각각 변경
  - 설치경로/scripts 폴더에서  pip.exe -> pip32.exe | pip64.exe 로 각각 변경

### 확인
  - cmd 오픈
  - python32 또는 python64 입력하면 버전에 따라 나오게 됨


### pip 설치방법 (예)
  - 64 비트: pip64 install tenserflow
  - 32 비트: pip32 install win32com
