---
title: "vscode Setting"
description: ""
date: "2020-07-16T10:24:55+09:00"
thumbnail: ""
categories:
  - "programs"
tags:
  - "vscode"
---
1. 확장 프로그램 설치
  * ESlint
  * Git Graph
  * GitLens - Git supercharged
  * Korean Language Pack for Visual code
  * Live Server
  * Vetur
  * vscode-icons
  * vue

2. 우분투에서 설치
  * 20.04 버전 기준
  * vscode 홈페이지에서 설치파일(.deb) 받아서 설치  
    ('ubuntu software'로 설치시 한글입력 안됨)
