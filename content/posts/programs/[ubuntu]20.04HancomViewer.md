---
title: "[Ubuntu]20.04 한컴뷰어 설치"
description: ""
date: "2020-08-19T11:39:04+09:00"
thumbnail: ""
categories:
  - "ubuntu 20.04"
tags:
  - ""
---
### 우분투 20.04 한컴뷰어 설치과정

* 의존성 문제로 한컴뷰어 인스톨 프로그램 설치 제대로 안됨
* 패키지 의존성 반영한 설치방법: gdebi
  ```
  sudo apt install gdebi
  ```
*  20.04에서는 위 방법으로도 인스톨 안됨 (율무님 포스트 참고)

* 의존성 패키지 다운로드
  ```
  libicu60 -> libjavascriptcoregtk -> libwebkitgtk
  ```
* gdebi 통해서 인스톨한후 성공
  ```
  sudo gdebi hancomoffice-hwpviewer-Ubuntu-amd64.deb
  ```



참고자료
 - [유르무차의 자기개발서]: https://yurmu.tistory.com/3
 - HiSEON: https://hiseon.me/linux/ubuntu/ubuntu-hwp-viewer/
