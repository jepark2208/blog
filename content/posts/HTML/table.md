---
title: "HTML - Table"
description: ""
date: "2020-05-13T18:01:37+09:00"
thumbnail: ""
categories:
  - "HTML"
tags:
  - "HTML"
---
### HTML 에서 테이블 생성하기
* 표 제목: <caption>
* 테이블 생성 태그: <table>
* <th>
* <thead>
* <tbody>
* <tfoot>
* 행 생성: <tr>
* 내용넣을때(table data cell): <td>


출처: https://webdir.tistory.com/317
