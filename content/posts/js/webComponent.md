---
title: "Web Component 로 모듈화 하기"
description: ""
date: "2021-10-31T15:16:14+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---

바닐라 자바스크립트에서 사용할 수 있는 웹 컴포넌트 내장함수 사용법
출처: 코딩애플 (https://youtu.be/RtvSgptpfnY)

 1. 내장함수로 사용됨: 별도 라이브러리 없음
 2. input, label, style 등 html 등록 가능
 3. 커스텀 attribute 넣기도 가능 : 다른 attribute 넣을 때마다 다른 HTML 내용 보여줄 수 있음
 4. 자동 HTML 재렌더링기능도 구현 가능

```HTML
</head>

<body>
  <!-- 3. 재렌더링기능 : name="123" -->
  <custom-input name="123"></custom-input>

<script>
  class CustomInput extends HTMLElement {
    connectedCallBack() {

      //2. html 만들어 주는 코드
      let 라벨 = document.createElement('label');
      라벨.innerHTML = '이름을 입력하쇼';
      this.appendChild(라벨);
      let 인풋 = document.createElement('input');
      this.appendChild(인풋);

      let 인풋2 = document.createElement('style');
      thisappendChild(인풋2);
    }

    // 4. 재렌더링기
    static get observedAttributes(){ // 이런 attribute가 바뀌는지 감시(기본함수)
      return ['name']
    }
    attributeChangeCallback(){ // 바뀌면 이거 실행됨(일종의 훅 기능)

    }
  }

  //웹 컴포넌트 생성
  comtomElement.define('custom-input', customInput)

</script>
```
