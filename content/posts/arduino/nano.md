---
title: "아두이노 나노 호환보드 CH340"
description: ""
date: "2020-07-26T14:50:20+09:00"
thumbnail: ""
categories:
  - "arduino"
tags:
  - ""
---
### 아두이노 나노 호환보드 연결
아두이노 ide 프로그램에서 인식 불가로 구매사이트에서 찾아보니
호환보드임을 알게됨.  
```link
아두이노 나노 호환보드 CH340 [SZH-EK025] 다운로드
https://sparks.gogo.co.nz/ch340.html
```
위 링크에서 드라이버를 받고, 나노를 연결 후 드라이버를 설치하면
설치됨.

2020.7.27 우분투에서 확인 결과 CH341인거 같다.
우분투에서 arduino IDE 실행은 다운로드 받고 적당한 곳에 압축을 푼 다음 sudo로 실행.
관련 내용은 아래 참조
```terminal
// 포트번호 확인
dmesg | grep tty

//포트 리스트
ls -l /dev/ttyUSB0

// 포트 상태 확인
sudo stty -F /dev/ttyUSB0

```
### 아두이노 나노로 hello world 출력해보기
```arduino
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Hello World");
  delay(5000);
}

```
