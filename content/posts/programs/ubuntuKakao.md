---
title: "우분투 20.04 카카오톡 설치"
description: ""
date: "2020-07-24T12:52:10+09:00"
thumbnail: ""
categories:
  - "programs"
tags:
  - "Ubuntu"
  - "KakaoTalk"
---
## 가능한 방법
  * PlayOnLinux 로 설치: 방법은 간단하고 UI 가 잘 되어 있으나 프로그램을 2개 띄워야 함  
    (PlayOnLinux + 카카오톡)
    참고자료: https://tolovefeels.tistory.com/6
  * wine 프로그램을 설치: 설정이 복잡하나 바로가기, 아이콘 등 설정 잘됨

### wine을 사용한 방법

* 32비트 설정(?)  
  wine 64비트는 호환성이 별로라 프로그램 실행이 잘 안될수도 있다고 함
  ```terminal
    sudo dpkg --add-architecture i386
    sudo wget -nc https://dl.winehq.org/wine-builds/Release.key
    sudo apt-key add Release.key
  ```

* 버전에 따라 저장도 다르게 추가 (20.04 만 복사함)
  ```terminal
  sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'

  sudo apt update
  ```
* 안정화 버전으로 설치 (stable)
  ```terminal
  sudo apt install --install-recommends winehq-stable
  ```
* 카카오톡 설치
  ```terminal
  sudo apt-get install fonts-nanum*
  ```
* 설치확인
  ```terminal
  which wine
  wine --version
  ```

* $HOME 폴더에 ./ wine 폴더 생성
  ```terminal
  WINEARCH=win32 winecfg
  ```

* winetricks 설치,  DLL과 라이브러리 설치
  ```terminal
  sudo apt-get install winetricks
  winetricks gdiplus wmp9 riched20 riched30 d3dx9_43
  ```

* 카카오 설치 프로그램 실행  
  (다운받은 폴더, xp버전으로 받아야함)
  ```terminal
  wine KakaoTalk_setup.exe
  ```

  ### 추후 확인사항
  * 일반 윈도우 버전(xp버전 말고)으로 실행시 로그인은 되나 채팅창 열면 꺼짐
    (와인 최신버전으로 하면 될수도 있을거 같은데 못하겠음)
  * 채팅창에서 파일 다운로드는 안됨
  * 파일 다운로드할 경우는 게시판 이용
  * 이모티콘 안됨
  -

-------------------------------
  * 설치일자: 2020.07.24
  * 참고자료:
  1. https://joelweon.github.io/2019/05/08/linux-wine-kakao-install.html
  2. https://velog.io/@filoscoder/ubuntu-18.04-wine-%EC%99%84%EC%A0%84%ED%9E%88-%EC%A0%9C%EA%B1%B0%ED%95%98%EA%B8%B0
  3. https://joelweon.github.io/2019/05/08/linux-wine-kakao-install.html
  4. /....
