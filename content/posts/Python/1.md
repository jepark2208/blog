---
title: "1"
description: ""
date: "2021-08-06T08:51:55+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---

https://wikidocs.net/85 참고  
```
>>> class Singer:                      # 가수를 정의하겠느니라…
...     def sing(self):                # 노래하기 메서드
...            return "Lalala~"
...    
>>> taeji = Singer()                   # 태지를 만들어랏!
>>> taeji.sing()                       # 노래 한 곡 부탁해요~
'Lalala~'

>>> ricky = Singer()
>>> ricky.sing()
'Lalala~'
```

변수와 매서드  

```
class Amazon:
    strength = 20
    dexterity = 25
    vitality = 20
    energy = 15

    def attack(self):
        return 'Jab!!!'

>>> import diablo2
>>> jane = diablo2.Amazon()
>>> mary = diablo2.Amazon()

>>> jane.strength
20
>>> jane.attack()
'Jab!!!'
```
