---
title: "gitlab CI/CD 사용해보기 - 2. 개발서버"
description: ""
date: "2022-09-02T17:18:59+09:00"
thumbnail: ""
categories:
  - "server"
tags:
  - "sever"
  - "Apache"
---

### 개발된 웹 소프트웨어 정보

이클립스에서 전자정부로 된 소스를 localhost 로 돌릴때 관련 정보는 다음과 같다.

- OpenJDK 1.8
- eGovFrame
- Tomcat 9.0.63 (?)

### 개발서버를 구성하기

- 별도의 컴퓨터 및 클라우드 서버 자원이 없으므로 48core 컴퓨터의 도커 컨테이너로 구성하기로 함

### 아마도 3번의 시행착오를 겪음. 문제는 따로 있었지만 나름의 장단점이 있어 다 언급하고자 함.

- 1. dockerhub 의 apache 이미지를 pull 받아서 사용. 본문 내용을 잘 보고 jdk 및 tomcat 버전에 맞는 Dockerfile을 사용한다.
- 2. centOs 7 컨테이너를 만들어 컨테이너 내부에서 (docker exec~) jdk 및 apache를 설정
- 3. Ubuntu 20.04 컨테이너를 만들어 컨테이너 내부에서 (docker exec~) jdk 및 apasche 를 설정
- 4. Dockerfile 을 작성해서 커스텀한 컨테이너를 생성해서 수행 (결과적으로 성공)

### 위 항목대로 어디까지 했고, 어디서 막혔는지

1. openJdk 1.8 버전인데 jre8 버전으로 해서 소스가 돌아가지 않음. openJDK 에 맞는 설정으로 했을 시 성공했을지도..  
   돌아는 가지만 catalina.out 로그가 안찍히는 부분은 해결하지 못함.
2. centos7 에서 한글이 깨져서 나오는 부분을 해결하지 못해 프론트 소스는 돌아갔지만 catalina.out 에서 한글로 된 로그가 깨짐  
   도커를 생성할때 한글이 깨지지 않게끔 따로 설정을 해 주어야 한다는데 다른 산을 팔거같아서 포기. 하지만 centos 가 안정적이라고 하니  
   추후 이거로 테스트 해 볼 필요는 있음. (왜 안정적인지는 들은말이라 이유는 모름)
3. 프론트 및 catalina.out 로그도 잘 된거 같은데 pom.xml 라이브러리 오류로 끝까지 확인해 보지는 못함. 아마 다시하면 잘 될거같음
4. Dockerfile 로 작성해서 커스텀 이미지로 된 컨테이너로 만들었고. 안되었지만 pom.xml 을 수정했어서 실질적으로 완성함

### 다음장 일정

- gitlab 및 gitlab-runner 연동
- 실패한 방법들 개발서버로 빌드 및 배포하는 gitlab-ci.yml 파일 작성
- 필요한 네트워크 관련 명령어 및 ssh 정리
