---
title: "테이블 정의서 추출 쿼리"
description: ""
date: "2022-12-29T20:19:29+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---

산출물 목록의 테이블 정의서 추출 쿼리  
첫번째로 검색한 블로그 자료가 잘 적용이 되어서 다행  
출처: https://chobopark.tistory.com/172  
출처에는 mysql으로 한 내용이고, mariaDB에서도 잘 적용됨  
주신 자료 덕분에 잘 해결했고 배웠습니다. 감사합니다

### 1. 쿼리문

```sql
  SELECT
    t1.table_name, t1.table_comment, column_name, data_type, column_type, column_key, is_nullable, column_default, extra, column_comment
  FROM
    (SELECT
        table_name, table_comment
      FROM
        information_schema.TABLES WHERE table_schema='스키마명') t1,
    (SELECT
        table_name, column_name, data_type, column_type, column_key, is_nullable, column_default, extra, column_comment, ordinal_position
      FROM
        information_schema.COLUMNS WHERE table_schema='스키마명') t2
  WHERE
      t1.table_name = t2.table_name
  ORDER BY
      t1.table_name, ordinal_position;
```

### postgresql 테이블정의서 추출 쿼리
- 출처: https://wogus789789.tistory.com/266
``` sql
SELECT
	info. TABLE_NAME,
	info. COLUMN_NAME,
	info.udt_name as type,
	case when info.character_maximum_length is null then info.numeric_precision else info.character_maximum_length end as length,
	info.column_default,
	info.is_nullable,
	comm.column_comment as comment,
	case when pri_key.column_name is null then '' else 'PK' end as PK
FROM
	information_schema. COLUMNS info
LEFT JOIN (
	SELECT
		PS.schemaname as SCHEMA_NAME,
		PS.RELNAME AS TABLE_NAME,
		PA.ATTNAME AS COLUMN_NAME,
		PD.DESCRIPTION AS COLUMN_COMMENT
	FROM
		PG_STAT_ALL_TABLES PS,
		PG_DESCRIPTION PD,
		PG_ATTRIBUTE PA
	WHERE
		PS.RELID = PD.OBJOID
	AND PD.OBJSUBID <> 0
	AND PD.OBJOID = PA.ATTRELID
	AND PD.OBJSUBID = PA.ATTNUM
	ORDER BY
		PS.RELNAME,
		PD.OBJSUBID
) comm ON comm.SCHEMA_NAME = info.table_schema
AND comm. TABLE_NAME = info. TABLE_NAME
AND comm. COLUMN_NAME = info. COLUMN_NAME
LEFT JOIN (
	SELECT
		CC.*
	FROM
		INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC,
		INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CC
	WHERE
		TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
   AND TC.TABLE_CATALOG   = CC.TABLE_CATALOG
   AND TC.TABLE_SCHEMA    = CC.TABLE_SCHEMA
   AND TC.TABLE_NAME      = CC.TABLE_NAME
   AND TC.CONSTRAINT_NAME = CC.CONSTRAINT_NAME
) pri_key ON pri_key.table_schema = info.table_schema
AND pri_key. table_name = info.TABLE_NAME
AND pri_key. column_name = info. COLUMN_NAME
WHERE
	info.table_schema = 'public'
ORDER BY
	info. TABLE_NAME,
	info.ordinal_position;
```
### 2. 스키마

- 스키마란 데이터베이스 이름이라고 함.  
  DBeaver 에 Database 하위에 디스크 아이콘 명
