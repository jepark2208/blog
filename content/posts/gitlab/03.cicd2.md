---
title: "ci/cd maven project"
description: ""
date: "2022-09-02T17:18:59+09:00"
thumbnail: ""
categories:
  - "server"
tags:
  - "sever"
  - "Apache"
---

### cicd1 구성을 사용하지 않는 것으로 함

- 사유: gitlab-runner 를 운영서버에 설치한 케이스임(ncloud)
- 이 경우 서버 자원에 부담이 많이 간다고 함 (gitlab-runner 가 리소스를 많이 잡아먹음)

### gitlab-runner 컨테이너 내부의 maven 설정 관련 위치

```
/etc/maven/m2.conf
/etc/maven/settings.xml
/etc/maven/toolchains.xml

/opt/maven -> /opt/apache-maven-3.8.6
/opt/maven/LICENSE
/opt/maven/NOTICE
/opt/maven/README.txt
/opt/maven/bin/m2.conf
/opt/maven/bin/mvn*
/opt/maven/bin/mvn.cmd
/opt/maven/bin/mvnDebug*
/opt/maven/bin/mvnDebug.cmd
/opt/maven/bin/mvnyjp*
/opt/maven/boot/plexus-classworlds-2.6.0.jar
/opt/maven/boot/plexus-classworlds.license
/opt/maven/conf/.settings.xml.swp
/opt/maven/conf/logging/
/opt/maven/conf/settings.xml
/opt/maven/conf/toolchains.xml
/opt/maven/lib <- *.jar 파일이 많이 있음
```

-rw-r--r-- 1 root root 16384 Nov 14 10:04 .settings.xml.swp
drwxr-xr-x 2 root root 4096 Jun 6 16:16 logging/
-rw-r--r-- 1 root root 10742 Jun 6 16:16 settings.xml
-rw-r--r-- 1 root root 3747 Jun 6 16:16 toolchains.xml

### 내부망 컨테이너에 올라가 있는 gitlab-runner 재확인

- gitlab-runner 컨테이너 내 bash 로 들어가서 신규 gitlab-runner 등록

```
gitlab-runner register
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://<Gitlab URL 주소>/
Enter the registration token:
<Gitlab 프로젝트의 Token을 입력>
Enter a description for the runner:
[6e4c57b3ff4f]:  gitlab-runner
Enter tags for the runner (comma-separated):
sdh-tf-runner (필수 x)
Registering runner... succeeded                     runner=s-EJMiEU
Enter an executor: docker-ssh, parallels, shell, virtualbox, docker-ssh+machine, kubernetes, custom, docker, ssh, docker+machine:
docker
Enter the default Docker image (for example, ruby:2.6):
docker-latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
(참고: https://seodae.tistory.com/m/37)
```

- 해당 프로젝트의 settings > ci/cd > expand 로 들어가면 gitlab-runner 가 연동되어 있음을 알 수 있다.

- config.toml 수정  
  config.toml 이 docker 로 되어있으면 안됨, 처음부터 excutor 를 shell로 해야함  
  docker 로 excutor 하면 신규 컨테이너를 땡겨와서 거기서 만들기 때문에 일련의 흐름을 파악하기 힘듬

  ```
  [[runners]]
    name = "jdk-egov-project"
    url = "***********"
    token = "*************"
    executor = "shell"
    builds_dir = "/home/gitlab-runner/build"
    [runners.cache]
      [runners.cache.s3]
      [runners.cache.gcs]
      [runners.cache.azure]
  ```

  - 프로젝트 폴더의 gitlab-ci.yml 파일로 빌드부터 실행

  ```
  stages:
  - build

  build:

    stage: build

    script:
      #- mvn clean install
      - id
      - who am i
      - pwd
      - ls -a -l
      #- mvn clean package -Dmaven.test.skip=true
      - mvn clean
      - echo 'maven clean success'
      - mvn package -Dmaven.test.skip=true


    after_script:
      - echo 'build success!'
    tags:
      - jdk

  ```

- 테스트 해본 gitlab-cy.yml 명령어 실행 결과
  ```
  $ id
  uid=999(gitlab-runner) gid=999(gitlab-runner) groups=999(gitlab-runner)
  $ who am i
  $ pwd
  /home/gitlab-runner/build/zgsBEL3p/0/kobaco/psa/application
  $ ls -a -l
  ```
- mvn clean 명령어는 실행 성공함

- mvn package -Dmaven.test.skip=true 는 실패  
  문구는 아래와 같음

  ```
  [WARNING] 'dependencies.dependency.systemPath'
  ```

- 진행사항:

  - 1. offline 모드로 실행하기 위한 세팅을 수행 (컴파일 시간을 단축할 수 있을거 같음)
  - 2. gitlab-ci.yml 에서 pom.xml 파일 있는곳으로 이동하는 스크립트 추가

  - 1.1 offline 모드로 설정하기
    프로젝트에 필요한 의존성 목록들 다운로드 하기  
    pom.xml 이 있는 폴더에 들어가서 아래 명령어를 입력한다.

  ```
  $ mvn dependency:go-offline
  ```

  - 1.2 오프라인 모드 설정

  ```
  mvn -o verify
  ```

  - 1.2 실패, 컨테이너 메이븐 버전이 3.6, 프로젝트의 메이븐 버전이 3.8 이어서 컨테이너 메이븐 버전을 맞춰볼 예정

- 해결함
  - 원인: 메이븐이 pom.xml 기반으로 다운로드 받을 시 http를 https 로 변경해주는 작업들이 필요.
  - psa의 pom.xml을 토대로 구글링해서 https 로 변환하는 작업을 수행 후 mvn install 명령어를 실행하니 war 파일이 잘 만들어짐

### 개발서버 세팅

- 도커로 tomcat 이미지를 사용하여 개발서버를 만들었음
- 단순히 docker pull tomcat 으로 컨테이너를 생성하면 jdk 버전이 war 파일과 맞지 않아 실행이 안됨
- dockerhub 의 공식 tomcat 페이지의 overview를 확인하여 jdk 버전과 맞는 dockerfile로 이미지를 생성 후 컨테이너를 만들어야 함
- https://hub.docker.com/_/tomcat
- 사용하고자 하는 war 파일은 tomcat 9 및 openjdk 1.8 버전을 사용하고 있어 해당하는 버전의 Dockerfile을 생성해서 이미지를 만들었음
- 찾아보니 jdk 1.8 = jdk 8 이라고 함
- 도커파일 출처:
  https://github.com/docker-library/tomcat/blob/33f29375055a9bcb35b3d906a435eefa9c7d671e/9.0/jdk8/temurin-focal/Dockerfile

```
### 48코어 서버pc에 도커파일 경로를 대략 설정
mkdir /home/dockerfiles/tomcat_9.0_jdk8_temurin-focal
vi Dockerfile

### 도커파일 작성 후
docker build -t tomcat9jdk8:0.1 .

### 생성된 이미지 확인
docker images

```

- 생성된 이미지로 컨테이너도 생성, 포트오픈은 7070 -> 8080 으로 수정함
- 테스트용 war 파일을 수동으로 넣어봤는데 압축이 풀리면서 테스트페이지 접속 성공

```
docker cp ROOT.war tomcat9jdk8:/usr/local/tomcat/webapps
```

- 접속 경로는 도커가 설치된 서버 피씨의 ip에 설정한 포트로 접속하면 됨 (192.168.**.\***:7070)

### gitlab-ci.yml 파일에 deploy 를 scp로 설정하기

#### scp 테스트

### 컨테이너끼리 ssh 등록해서 비밀번호 없이 전송하기 도전

```
sudo apt-get update
sudo apt-get upgrade
sudo apt install openssh-server
sudo apt install vim


```

- gitlab-runner 그룹 및 사용자 추가

```
groupadd gitlab-runner
adduser -g gitlab-runner gitlab-runner
```

- 개발서버에 gitlab-runner, root 비밀번호 설정

```
passwd root
passwd gitlab-runner
```

- ssh 연결 관련 참고자료
  https://2mukee.tistory.com/250

- ssh 연결 수정시 기존 유저 접속정보를 삭제해야함
  A -> B 로 접속시 ssh 오류일 경우 A 터미널이서 수행  
  ip는 B의 자료를 입력
  해당 내용은 known_host 파일 내용을 지우는 것이라고 함

```
ssh-keygen -R 123.123.123.123
```

- tomcat ROOT 디렉토리 변경하기

  - vi 톰캣경로/conf/server.xml
  - <Host name="localhost" appBase="원하는 경로로 수정"
  - <Context docBase="/home/psa/uploadfile" path="/uploadfile/" reloadable="true"/> 필요하면 추가

  - 참고: https://imthekingofcoding.tistory.com/13
