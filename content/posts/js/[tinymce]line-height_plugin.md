---
title: "[tinyMCE] 줄간격(line-height) 플러그인"
description: ""
date: "2020-08-18T21:14:18+09:00"
thumbnail: ""
categories:
  - "tinyMCE"
tags:
  - "tinyMCE"
---
깃허브에 줄간격 플러그인이 있지만 v5 에서는 인식 불가.
플러그인을 찾아서 제작함

플러그인 예제 확인  
출처: https://www.tiny.cloud/docs-4x/advanced/creating-a-plugin/  

```javascript
// 플러그인 추가 시작방법
tinymce.PluginManager.add('lineheight', function(editor, url) {
  //에디터 툴바 설정 -addSplitButton 으로 생성(드롭다운)
  editor.ui.registry.addSplitButton('lineheight', {
      //툴바명 설정, 아이콘으로도 설정가능
      text: 'Line-height',
      onAction: function () {
        // 버튼 이름 클릭시 수행내용
        // 현 에디터에서는 사용하지 않아서 비활성화함
        //editor.insertContent('<p>Its Friday!</p>') ->에디터 내 글 삽입시
      },
      onItemAction: function (api, value) {
        // addSplitButton 드롭다운 내용을 선택했을때
        var defaultLineHeightFormats = '1 2 3 1.5 1.75 2 3 4 5';
        var userSettings = editor.settings.lineheight_formats;
        var lineheightFormats = typeof userSettings === 'string' ? userSettings : defaultLineHeightFormats;

        lineheightFormats.split(' ').map(item => {
          //'line-height' css 기본 문법(?) 을 적용함. 먼저 register를 해야 사용 가능
          editor.formatter.register(item, {inline: 'span', styles: { 'line-height': item, display: 'inline-block' } });
        })
        // 위에서 register한 설정을 적용할때 사용
        editor.formatter.apply(value)
      },
      fetch: function (callback) {
        //에디서 설정의 init 에서 추가한 내용을 반영
        var defaultLineHeightFormats = '1 2 3 1.5 1.75 2 3 4 5';
        var userSettings = editor.settings.lineheight_formats;
        var lineheightFormats = typeof userSettings === 'string' ? userSettings : defaultLineHeightFormats;
        //'items': 배열을 설정해 해당 내용을 onItemAction 으로 넘김
        var items = lineheightFormats.split(' ').map(item => {
          let text = item//, //string '1', '2', ...

          return {
            type: 'choiceitem',
            text: text,
            value: text
          };
        });

        callback(items);
      }
// Adds a menu item, which can then be included in any menu via the menu/menubar configuration
// 상단 메뉴에 추가하는 항목, 툴바랑은 다르다.
editor.ui.registry.addMenuItem('lineheight', {
  text: 'lineheight plugin',
  context: 'format',
  onAction: function() {
    // Open window
    openDialog();
  }
});
}
return {
      getMetadata: function () {
        return  {
          name: "lineheight plugin",
          url: "http://exampleplugindocsurl.com"
        };
      }
    };
  });  
```
### 추가 설정
* 플러그인 이름은 plugin.min.js 로만 인식 가능(왜인지는 모름)
* 에디터 init 에서 원하는 설정으로 인식 가능

### 이 작업을 하면서 느낀점
* 제조사 문서를 잘 찾아야 한다.
* 일단 작성해보고 경우의수를 늘려간다.
