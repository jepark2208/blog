---
title: "윈도우 10 설치한 프로그램"
description: ""
date: "2021-05-03T09:02:45+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---

###윈도우 10 설치시 내가 쓰는 프로그램

#### - 오피스 프로그램
1. MS office
2. 한글
3.

#### - 프로그래밍 프로그램
1. Vscode
1. Atom (blog용)
1. Eclipse
1. HeidiSQL (MariaDB)
1. Arduino
1. DBeaver
1. Fiddler
1. PuTTY
1. git

#### - 유틸 프로그램
1. 크롬 브라우저
1. 오페라 브라우저 (깃랩용)
1. Q-dir
1. PowerToys

#### - 취미용
1. 4k video downloader / youtube to mp3
1. FreeCAD
