---
title: "Function1"
description: ""
date: "2020-06-23T13:19:43+09:00"
thumbnail: ""
categories:
  - "js & jquery"
tags:
  - "JS"
---
## 자바스크립트 함수 관련
* 대괄호 안에 함수 선언  
  - 장점: IDE 편집기에서 기호로 되어있으면 묶어서 관리 가능

* 리턴값 유무  
  - 함수 선언시 각 코드는 실행되나 리턴값에 따라 활용여부가 다름

* 예시참조
```javascript
{
  function test () {console.log("fntest"); return "test1";}

}
var aaa = test (); //console: fntest
console.log(aaa); //console: test1
```
