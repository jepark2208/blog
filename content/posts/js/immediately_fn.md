---
title: "즉시실행함수"
description: ""
date: "2020-05-09T18:12:33+09:00"
thumbnail: ""
categories:
  - "js & jquery"
tags:
  - "JS"
---

### 즉시실행함수 (Immediately-invoked function expression)

1. 사용형태
    ```javascript
    (function() {
        console.log('함수 호출'); // "함수 호출" 출력
    }());
    ```
2. 사용이유  
  - 내부에만 사용, 공유될 필요 없는 속성이나 메소드가 있을때
  - 다른 스크립트 파일 내에서 동일한 이름의 변수나 함수의 충돌을 피하고 싶을때  



> 참고 및 원문  
> https://devyj.tistory.com/9  
> https://beomy.tistory.com/9
