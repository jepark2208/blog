---
title: "vuexy: 2. 페이지 레이아웃 및 컴포넌트 배치"
description: ""
date: "2020-09-28T10:26:44+09:00"
thumbnail: ""
categories:
  - "React"
tags:
  - "vuexy"
---

### 페이지 폴더 생성
- 1.에서는 페이지 생성여부만 확인했기 때문에 단일 js 파일만 있었으나 실제 페이지는
더 복잡한 구성을 가지므로 폴더구조로 변환

- src/views/pages 에 페이지별로 폴더 생성


### 예제에서 ListViews 페이지를 생성해보자
- ListViews.js 를 그대로 복붙하면 오류가 뜬다. 파일 채로 복사한다
- src/assets/scss/pages/data-list.scss
- src/redux/actions/data-list 폴더
