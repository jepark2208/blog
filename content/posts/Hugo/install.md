---
title: "Install"
description: ""
date: "2020-05-20T17:34:22+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---
1. 설치 안내 페이지(설치 및 윈도우 환경변수)  
https://gohugo.io/getting-started/installing/

2. 설치파일 링크  
https://github.com/gohugoio/hugo/releases  

3. 사용법 링크(사이트 생성)  
https://gohugo.io/getting-started/quick-start/  

4. 테마 설치 및 프로그램 설정  

5. 포스트 생성
```
hugo new post/example_category/example.md
```
6. 마크다운 사용법  
