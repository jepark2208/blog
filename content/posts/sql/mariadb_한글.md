---
title: "MariaDB - 한글깨짐 수정"
description: ""
date: "2020-05-15T14:47:33+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---
마리아DB 에 인코딩 설정 문제로 한글입력이 되지 않음을 확인하고 자료를 찾아서 기록함.  

### 인코딩 상태 확인
```
mysql> show variables like 'c%';
```

### mysql config 파일의 인코딩 속성 수정
* 터미널 접속 및 mysql 파일 찾기  
$ whereis mysql

* 위 경로는 압축파일? 만 있는거 같아 설정파일로 재검색  
$ whereis my.conf

* 설정파일 열어서 아래 내용 추가
$ vi /etc/my.cnf

```
[mysql]
default-character-set = utf8

[client]
default-character-set = utf8

[mysqld]
init_connect = "SET collation_connection = utf8_general_ci"
init_connect = "SET NAMES utf8"
character-set-server = utf8
collation-server = utf8_general_ci
```

### MariaDB 재실행
$ sudo service mariadb restart
//mysql의 경우
$ sudo service mysql restart

### 확인  
DB에 접속해서 맨 위의 show variable 을 다시 실행해서 utf8이 나오는지 확인.  

기존에 만든 테이블은 적용이 안되어 있으므로 삭제하고 다시 테이블을 만들어야 함.

가져온곳:  
https://hongsii.github.io/2017/10/15/mysql-%ED%95%9C%EA%B8%80-%EA%B9%A8%EC%A7%90-%EB%AC%B8%EC%A0%9C/
https://kyome.tistory.com/134
