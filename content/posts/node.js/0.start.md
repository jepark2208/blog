---
title: "0. node.js 시작"
description: ""
date: "2020-05-12T19:30:49+09:00"
thumbnail: ""
categories:
  - "node.js"
tags:
  - "node.js"
---
### 설치
* 20.5.12기준 가장 안정성 높은 버전인 12.x로 설치
* 윈도우: node.js 홈페이지에서 다운로드 후 설치파일 실행
* 우분투: 터미널에서 아래 명령어 입력
```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```
출처: https://github.com/nodesource/distributions/blob/master/README.md  
