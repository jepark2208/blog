---
title: "Setting"
description: ""
date: "2020-05-10T20:14:50+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""

---
사용한 테마: mainroad  
세팅해야할것: Mermaid  
(참고사이트: https://marvintan.com/posts/mermaid-in-hugo/#example-1)  

https://marvintan.com/posts/mermaid-in-hugo/  
https://osgav.run/lab/hugo-mermaid-diagrams.html  
https://mermaid-js.github.io/mermaid/#/usage

{{< mermaid >}}
sequenceDiagram
    participant Alice
    participant Bob
    Alice->John: Hello John, how are you?
    loop Healthcheck
        John->John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail...
    John-->Alice: Great!
    John->Bob: How about you?
    Bob-->John: Jolly good!
{{< / mermaid >}}
