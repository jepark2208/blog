---
title: "2. 쿠버네티스 설치하기"
description: ""
date: "2020-11-11T12:25:28+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---
### 개요
- Machine 1: Ubuntu 20.04, k8s-master, 192.168.10.5
- Machine 2: Ubuntu server 20.04?, k8s-node-0, 192.168.10.19

### Step1) Set hostname and add entries in /etc/hosts file
```cmd
$ sudo hostnamectl set-hostname "k8s-master"     // Run this command on master node
$ sudo hostnamectl set-hostname "k8s-node-0"     // Run this command on node-0
```  
호스트 파일에 아래 내용을 추가(디바이스별)  
```cmd
192.168.10.5     k8s-master
192.168.10.19    k8s-node-0
```

### Step 2) Install Docker (Container Runtime) on all 3 nodes
```cmd
$ sudo apt update
$ sudo apt install -y docker.io
```

docker 서비스 활성화 확인
```cmd
$ sudo systemctl enable docker.service --now
```

(active 되어있는지 확인한다.)
```cmd
$ systemctl status docker
$ docker --version
```

### Step 3) Disable swap and enable IP forwarding on all nodes
```cmd
$ sudo vi /etc/fstab
```
아래의 파일에서 swapfile 줄 주석처리
```
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda2 during installation
UUID=5d56a02c-698d-4933-9ecf-1f669d4488c7 /               ext4    errors=remount-ro 0       1
# /boot/efi was on /dev/sda1 during installation
UUID=2B5E-7210  /boot/efi       vfat    umask=0077      0       1
# /swapfile                                 none            swap    sw              0       0
# 윗줄 주석
~
```

swapoff 명령어
```cmd
$ sudo swapoff -a
```

ip forwarding 영구고정? 을 위한 설정
```cmd
$ sudo sysctl -p
net.ipv4.ip_forward = 1
$ # 위 메시지 나오는지 확인
```

### Step 4) Install Kubectl, kubelet and kubeadm on all nodes
마스터, 노드 등 모든 디바이스에 설치

```cmd
$ sudo apt install -y apt-transport-https curl
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
$ sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
$ sudo apt update
$ sudo apt install -y kubelet kubeadm kubectl
```

### Step 5) Initialize Kubernetes Cluster using kubeadm (from master node)
쿠버네티스 이닛, 명령어 입력 후 나오는 메시지 저장해둘것(필수)

```cmd
$ sudo kubeadm init
```

```cmd
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


kubeadm join 192.168.10.5:6443 --token **** \
  --discovery-token-이하생략
```

위 명령어 중 폴더생성 관련 터미널 복붙
```cmd
pkumar@k8s-master:~$  mkdir -p $HOME/.kube
pkumar@k8s-master:~$  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
pkumar@k8s-master:~$  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

노드에 마스터 연결 (kubeadm join) <- 이닛할때 나온 결과물 복붙

```cmd
pkumar@k8s-node-0:~$ sudo kubeadm join 192.168.10.5:6443 --token b4sfnc.53ify이하생략
```

설정이 완료되면 마스터에서 "kubectl get nodes" 명령어로 디바이스 확인
```cmd
$ kubectl get nodes
NAME         STATUS     ROLES    AGE    VERSION
k8s-master   NotReady   master   129m   v1.19.3
k8s-node-0   NotReady   <none>   39s    v1.19.3
```

조인할 때 오류가 발생했는데 나같은 경우는 마스터 방화벽에서 192.168.10.5:6443 허용하니 해결됨
```cmd
$ sudo ufw status
상태: 활성

목적                         동작          출발
--                         --          --
22/tcp                     ALLOW       Anywhere
6443                       ALLOW       Anywhere
22/tcp (v6)                ALLOW       Anywhere (v6)
6443 (v6)                  ALLOW       Anywhere (v6)

```

(참고 6443 포트 허용)
```cmd
sudo ufw allow 6443
```


출처: https://www.linuxtechi.com/install-kubernetes-k8s-on-ubuntu-20-04/
