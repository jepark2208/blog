---
title: "CentOS 에 Mariadb 설치"
description: ""
date: "2020-05-13T20:19:29+09:00"
thumbnail: ""
categories:
  - ""
tags:
  - ""
---
넷북에 설치한 CentOS에 MariaDB 설치  
MariaDB 사이트에서의 방법으로는 뭔가 안되었음.  

### 1. 설치
* putty 또는 터미널에서 ssh로 서버컴퓨터 접속
* 설치 명령어: sudo yum install mariadb-server
*


참조: https://www.cyberciti.biz/faq/how-to-install-mariadb-on-centos-8/

### 2. 설정
* MariaDB 서비스를 부팅시 자동실행 설정하기  
  ```
  systemctl enable mariadb
  ```
* MariaDB 시작  
  ```
  systemctl start mariadb
  ```
* MariaDB의 root암호 및 기본보안설정 명령어  
  ```
  mysql_secure_installation
  ```
  -처음 설정일 경우 ENTER키  
  -Set root password  
  관리자인 root 계정의 비밀번호 사용여부 → Y  
  루트계정의 비밀번호를 지정한다
  -Remove anonymous users?  
  익명 사용자의 접속여부,  
  n → $mysql -u root 및 $mysql도 사용 가능함  
  되도록 Y를 선택하라고 함  
  -Disallow root login remotely?  
  127.0.0.1, localhost 외 다른 IP로 root 접속 가능여부  
  Yes 선택하면 원격으로 root 계정 접근을 막는다고 함.
  → No 선택  
  -Remove test database and access to it?  
  마리아DB 설치시 기본적으로 제공되는 test 데이터베이스의 삭제 여부 → 필요에 따라 선택할것  
  -Reload privilege tables now?  
  마지막, 프리빌리지(privilege) 테이블의 재시작 여부,  
  꼭 Yes를 선택하여 작업을 진행하라고 함

### 3. 실행, 테이블 생성 및 접근권한 추가

* 3306 포트 포트포워딩 설정 → 공유기 설정메뉴
* 3306 포트 방화벽 설정 해제(방화벽 글 참조)  
* 실행: $ mysql -uroot --p  
  mysql> use mysql;
* 데이터베이스 생성:  
  mysql> create database 'DB명';
* 데이터베이스 권한 설정  
  mysql> create user ''DB명''@'%' identified by '비밀번호';  

  mysql> grant all privileges on 'DB명'.* to '사용자명'@'%'; //특정 DB 지목  
  mysql> grant all privileges on *.* to '사용자명'@'%' identified by '비밀번호'; //모든 DB 권한설정  

  mysql> flush privileges;
  mysql> mysqladmin -u root -p reload;  
  mysql> show grants for '사용자명'@'%'; // 권한 확인

### 4. 로컬, 외부, 접근허용 관련
```
set password for 'root'@'localhost' = password('************');


## 유저 생성 (mysql, mariadb 동일)
create user 'root'@'localhost' identified by '*******';

create user 'root'@'%' identified by '*******';
create user 'root'@'127.0.0.1' identified by '*******';

ALTER USER 'root'@'localhost' IDENTIFIED BY '*******';
ALTER USER 'root'@'%' IDENTIFIED BY '*******';
ALTER USER 'root'@'127.0.0.1' IDENTIFIED BY '*******';
FLUSH PRIVILEGES;

grant all privileges on *.* to 'root'@'localhost';
grant all privileges on *.* to 'root'@'%';
grant all privileges on *.* to 'root'@'127.0.0.1';
```