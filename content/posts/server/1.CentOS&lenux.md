---
title: "서버구성 - 1. OS설치 및 설정"
description: ""
date: "2020-05-09T20:08:37+09:00"
thumbnail: ""
categories:
  - "server"
tags:
  - "server"
  - "centOS"
---

## 설치한 컴퓨터  
10년된 ACER 넷북... (그래도 듀얼코어 4gb 램..)  

## OS  
CentOS 8 버전 gui  

# 리눅스 vi 사용
vi [filename.extension]  
파일이 없으면 해당 파일명으로 생성된다.
* 처음 시작시 명령모드, 제일 하단에 파일멸, 줄 길이 나옴
* 내용입력: 명령 모드에서 a 입력후 타이핑
* 명령모드→입력모드(insert): esc
* 저장후 종료: 입력모드에서 ':wq' //write quit
* 저장안하고 종료: 입력모드에서 ':q!'

# 설정  
* 한글설치  
터미널 root 계정에서 아래 항목 입력 후 설치  
```
dnf install ibus-hangul
```
* Apache 설치 (버전2.4.37)  
Apache 홈페이지: https://httpd.apache.org/  
(추가할것: PHP+mariadb)
```
# Apache 설치
yum -y install httpd*      

# PHP 설치
yum -y install php php-mysqlnd;

# mariadb 설치
yum -y install mariadb*    
```
* Tomcat 설치  
Tomcat 홈페이지:
