---
title: "vuexy: 1. 메뉴 구성 및 페이지 생성"
description: ""
date: "2020-09-28T10:26:44+09:00"
thumbnail: ""
categories:
  - "React"
tags:
  - "vuexy"
---

### 왼쪽 메뉴 만들기
- 경로: src/configs/navigationConfig.js
- 구성
      ```javascript
      {
        id: "termSearch",
        title: "용어 검색",
        type: "item",
        icon: <Icon.ZoomIn size={20} />, //icon 다수 있음
        permissions: ["admin", "editor"],
        navLink: "/"
        },
      ```

### 메뉴별 페이지 생성
- 경로: src/views/pages/페이지명.js
- 구성
      ```javascript
      import React from "react"

      class TermSearch extends React.Component{
        render(){
          return <h4>termSearchdkfdkjfkdajfkasdf</h4>
        }
      }

      export default TermSearch //첫글자는 대문자로
      ```


### Router.js 구성하기
- 위치: src/Router.js
- 구성
      ```javascript
      // Route-based code splitting
      const TermSearch = lazy(() =>
        import("./views/pages/termSearch")
      )
      ```
