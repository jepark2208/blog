---
title: "CentOS 방화벽"
description: ""
date: "2020-05-14T18:43:29+09:00"
thumbnail: ""
categories:
  - "server"
tags:
  - "CentOS8"
  - "linux"
  - "방화벽"
---
### 1. 설치, 실행, 중지
```
yum install firewalld //설치
systemctl start firewalld //방화벽시작
systemctl enable firewalld // 방화벽중지
firewall-cmd --reload // 방화벽재시작
```

기본 설정: /usr/lib/firewalld/zones  
시스템 개별설정: /etc/firewalld/firewalld.conf
사용자 zone설정: /etc/firewalld/zones/public.xml




원문: https://steady-hello.tistory.com/60
